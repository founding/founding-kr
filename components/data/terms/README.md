# data/terms

## Base

scoped-slot 패턴을 이용하여, 다른 terms 컴포넌트가 Base를 extends 한다.

scoped 값으로 `className`을 제공하고, `extends` 한 컴포넌트들은 이 클래스를 이용하여 텍스트를 꾸민다.

scss 파일은 레이아웃만을 꾸미며, 실제 색상등은 각 페이지에서 꾸미도록 한다.
