<!--
https://github.com/wyzant-dev/vue-radial-progress

MIT License

Copyright (c) 2016 Wyzant Inc

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
<template>
  <div
    :style="containerStyle"
    :class="getClasses('radial-progress')">
    <div
      :style="innerCircleStyle"
      :class="getClasses('inner')">
      <slot/>
    </div>
    <svg
      :width="diameter"
      :height="diameter"
      :class="getClasses('bar')"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg">
      <defs>
        <radialGradient
          :id="radialGradientId"
          :fx="gradient.fx"
          :fy="gradient.fy"
          :cx="gradient.cx"
          :cy="gradient.cy"
          :r="gradient.r">
          <stop
            :stop-color="startColor"
            offset="30%"/>
          <stop
            :stop-color="stopColor"
            offset="100%"/>
        </radialGradient>
      </defs>
      <circle
        :r="innerCircleRadius"
        :cx="radius"
        :cy="radius"
        :stroke="innerStrokeColor"
        :stroke-dasharray="circumference"
        :style="strokeStyle"
        :stroke-linecap="linecap"
        :fill="innerFillColor"
        stroke-dashoffset="0"/>
      <circle
        :transform="'rotate(270, ' + radius + ',' + radius + ')'"
        :r="innerCircleRadius"
        :cx="radius"
        :cy="radius"
        :stroke="'url(#'+radialGradientId+')'"
        :stroke-dasharray="circumference"
        :stroke-dashoffset="circumference"
        :style="progressStyle"
        :stroke-linecap="linecap"
        fill="transparent"/>
    </svg>
  </div>
</template>

<script>
/**
 * CircleProgress
 * @vuedoc
 * @classdesc 원 모양의 프로그레스 컴포넌트
 * @exports progress/Circle
 */
export default {
  name: 'CircleProgress',
  props: {
    id: {
      type: String,
      required: true
    },
    diameter: {
      type: Number,
      required: false,
      default: 200
    },
    totalSteps: {
      type: Number,
      required: true,
      default: 10
    },
    completedSteps: {
      type: Number,
      required: true,
      default: 5
    },
    startColor: {
      type: String,
      required: false,
      default: '#00a86b'
    },
    stopColor: {
      type: String,
      required: false,
      default: 'rgb(183, 247, 189)'
    },
    strokeWidth: {
      type: Number,
      required: false,
      default: 10
    },
    animateSpeed: {
      type: Number,
      required: false,
      default: 1000
    },
    innerStrokeColor: {
      type: String,
      required: false,
      default: '#323232'
    },
    innerFillColor: {
      type: String,
      required: false,
      default: 'gray'
    },
    fps: {
      type: Number,
      required: false,
      default: 60
    },
    timingFunc: {
      type: String,
      required: false,
      default: 'ease'
    },
    linecap: {
      type: String,
      required: false,
      default: 'round' // round, square, butt
    }
  },

  data() {
    return {
      gradient: {
        fx: 0.99,
        fy: 0.5,
        cx: 0.5,
        cy: 0.5,
        r: 0.65
      },
      called: 0,
      gradientAnimation: null,
      currentAngle: 0,
      strokeDashoffset: 0
    }
  },

  computed: {
    radius() {
      return this.diameter / 2
    },

    circumference() {
      return Math.PI * this.innerCircleDiameter
    },

    stepSize() {
      if (this.totalSteps === 0) {
        return 0
      }

      return 100 / this.totalSteps
    },

    finishedPercentage() {
      return this.stepSize * this.completedSteps
    },

    circleSlice() {
      return (2 * Math.PI) / this.totalSteps
    },

    animateSlice() {
      return this.circleSlice / this.totalPoints
    },

    innerCircleDiameter() {
      return this.diameter - this.strokeWidth * 2
    },

    innerCircleRadius() {
      return this.innerCircleDiameter / 2
    },

    totalPoints() {
      return this.animateSpeed / this.animationIncrements
    },

    animationIncrements() {
      return 1000 / this.fps
    },

    hasGradient() {
      return this.startColor !== this.stopColor
    },

    containerStyle() {
      return {
        height: `${this.diameter}px`,
        width: `${this.diameter}px`
      }
    },

    progressStyle() {
      return {
        height: `${this.diameter}px`,
        width: `${this.diameter}px`,
        strokeWidth: `${this.strokeWidth}px`,
        strokeDashoffset: this.strokeDashoffset,
        transition: `stroke-dashoffset ${this.animateSpeed}ms ${
          this.timingFunc
        }`
      }
    },

    strokeStyle() {
      return {
        height: `${this.diameter}px`,
        width: `${this.diameter}px`,
        strokeWidth: `${this.strokeWidth}px`
      }
    },

    innerCircleStyle() {
      return {
        width: `${this.innerCircleDiameter}px`
      }
    },

    radialGradientId() {
      return this.id
    }
  },

  watch: {
    totalSteps() {
      this.changeProgress({ isAnimate: true })
    },

    completedSteps() {
      this.changeProgress({ isAnimate: true })
    },

    diameter() {
      this.changeProgress({ isAnimate: true })
    },

    strokeWidth() {
      this.changeProgress({ isAnimate: true })
    }
  },

  created() {
    this.changeProgress({ isAnimate: false })
  },

  methods: {
    getStopPointsOfCircle(steps) {
      const points = []

      for (let i = 0; i < steps; i++) {
        const angle = this.circleSlice * i
        points.push(this.getPointOfCircle(angle))
      }

      return points
    },

    getPointOfCircle(angle) {
      const radius = 0.5

      const x = radius + radius * Math.cos(angle)
      const y = radius + radius * Math.sin(angle)

      return { x, y }
    },

    gotoPoint() {
      const point = this.getPointOfCircle(this.currentAngle)

      this.gradient.fx = point.x
      this.gradient.fy = point.y
    },

    changeProgress({ isAnimate = true }) {
      this.strokeDashoffset =
        ((100 - this.finishedPercentage) / 100) * this.circumference

      if (this.gradientAnimation) {
        clearInterval(this.gradientAnimation)
      }

      if (!isAnimate) {
        this.gotoNextStep()
        return
      }

      const angleOffset = (this.completedSteps - 1) * this.circleSlice
      let i = (this.currentAngle - angleOffset) / this.animateSlice
      const incrementer = Math.abs(i - this.totalPoints) / this.totalPoints
      const isMoveForward = i < this.totalPoints

      this.gradientAnimation = setInterval(() => {
        if (
          (isMoveForward && i >= this.totalPoints) ||
          (!isMoveForward && i < this.totalPoints)
        ) {
          clearInterval(this.gradientAnimation)
          return
        }

        this.currentAngle = angleOffset + this.animateSlice * i
        this.gotoPoint()

        i += isMoveForward ? incrementer : -incrementer
      }, this.animationIncrements)
    },

    gotoNextStep() {
      this.currentAngle = this.completedSteps * this.circleSlice
      this.gotoPoint()
    },

    getRadialGradientId() {
      if (process.server) {
        return 'radial-gradient' + this._uid
      } else {
        return $(this.$el)
          .find('radialGradient')
          .attr('id')
      }
    },

    getClasses(type) {
      var classes = {}
      switch (type) {
        case 'radial-progress':
          classes[this.$style['radial-progress']] = true
          break
        case 'inner':
          classes[this.$style['radial-progress__inner']] = true
          break
        case 'bar':
          classes[this.$style['radial-progress__bar']] = true
          break
      }
      return classes
    }
  }
}
</script>

<style module>
.radial-progress {
}

.radial-progress__inner {
}

.radial-progress__bar {
}

.radial-progress__inner + svg {
}
</style>
