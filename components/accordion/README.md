# accordion 컴포넌트

accordion 컴포넌트는 헤더와, 컨텐츠로 나눠지는 컨텐츠 컨테이너 컴포넌트이다.
헤더를 클릭하면 컨텐츠가 열리거나 닫히게 된다.

수평형, 수직형 두가지로 나뉘며, 각각 Item에 따라 성격이 변한다.

## 컴포넌트

### AccodionItem
 accordion 아이템을 담는 컨테이너로 `slot`으로 `header`, `content`를 담고 있다.

#### 프로퍼티 ####

### BaseAccordion
기본 accordion 컴포넌트로 `AccodionItem` 을 담는 컨테이너 역할을 한다.

