# 컴포넌트

## 디렉토리 구조
- 컴포넌트이름/Base.vue : 기본 뼈대가 되는 컴포넌트
- 컴포넌트이름/Abcd.vue : Base를 사용하여 만든 컴포넌트

## 규칙
- 컴포넌트는 반드시 필요한 것만 import 해야한다.

## 컴포넌트 디렉토리 이름 규칙

### 공통 규칙
- 컴포넌트 이름 앞에 '_' 이 있을 경우, 다른 컴포넌트에서 필요한 컴포넌트이다. 외부에서는 사용하지 못한다.

### plural(복수형)
- 가장 일반적으로 쓰이는 방식이다.
- 해당 컴포넌트로 분류되는 여러 컴포넌트가 있다.
- Base : 기본 베이스가 되는 컴포넌트
- 다른 컴포넌트 : Base를 기초로 한다.
- 컴포넌트 이름은 `component/Name`일 경우, `NameComponent` 로 설정된다.

### singular(단수형)
- 해당 컴포넌트의 메인은 Base가 되고
- 컴포넌트 이름 앞에 '_' 이 들어
- 컴포넌트->Base 입력
- Base : 메인 컴포넌트
- 다른 컴포넌트 : 메인 컴포넌트를 완전히 구성하기 위해서 필요한 컴포넌트이다.
- 컴포넌트 이름은 `component/Name` 일 경우, `ComponentName` 으로 설정된다. `Base`일 경우만 `BaseComponent` 가 된다.
