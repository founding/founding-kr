console.log(`SASS Compiling..`)
require('./sass-compile') // run sass-compile

const fs = require('fs')
const path = require('path')
const crypto = require('crypto')

console.log(`Generating Files..`)

const BUILDID = crypto.randomBytes(10).toString('hex')
console.log(`BUILD ID : ${BUILDID}`)
console.log(`Write File : .BUILDID`)
fs.writeFileSync(path.resolve(__dirname, '.BUILDID'), BUILDID)
