#+TITLE: Build 구조

* SCSS

** SCSS -> CSS 컴파일 (sass-compile.js)

- Development : static/tmp 위치에 생성
- Production : assets/css 위치에 생성

** 클래스 경량화 (nuxt.config.js)

- Development : 원본 클래스 명 사용
- Production : 경량화된 클래스명 사용

#+BEGIN_SRC js
build: {
  extractCSS: {
    allChunks: true
  }
}
#+END_SRC
위의 옵션 추가

** CSS import (*.vue)

- Development/Production : export function css 선언

- Development : 개발환경에서 적용하기 위해서는 아래 코드 필요
#+BEGIN_SRC js
if (nuxt.options.dev) {
  // dev 환경일 때 extractCSS 설정
  // css 파일 분리를 위함
  nuxt.hook('ready', () => {
    nuxt.options.build.extractCSS = {
      allChunks: true
    }
  })
}
#+END_SRC

** head() link (*.vue)

- Development : /tmp/ 로 자동 설정
- Production : /_nuxt/ 로 자동 설정
