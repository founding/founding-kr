export default function(ctx, inject) {
  /**
   * 중복하지 않는 랜덤 숫자를 생성한다.
   * max(50000)이상일 경우 처음부터 다시 재설정하게 된다.
   */
  var gen = function(scope = 'default') {
    var max = 50000 // 랜덤 숫자 범위
    var num = Math.floor(Math.random() * max)
    var history = ctx.store.state.random.history[scope] || []

    if (history.length == max) {
      // 무한 루프 방지 -> max 보다 더 많이 생성할 경우 중복이 될 수 있음
      ctx.store.commit('random/init', scope)
    }

    // 숫자 중복 방지
    while (history.indexOf(num) > -1) {
      num = Math.floor(Math.random() * max)
    }

    ctx.store.commit('random/add', {
      scope: scope,
      num: num
    })
    return num
  }
  inject('random', {
    gen: gen
  })
}
