const path = require('path')

module.exports = function nuxtToast(moduleOptions) {
  // Register plugin
  this.addPlugin({
    src: path.resolve(__dirname, 'plugins.js'),
    ssr: true,
    fileName: 'random.template.js',
    options: moduleOptions
  })
}
