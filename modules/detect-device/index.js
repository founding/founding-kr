const path = require('path')

module.exports = function nuxtToast(moduleOptions) {
  // Register plugin
  this.addPlugin({
    src: path.resolve(__dirname, 'plugins.js'),
    ssr: true,
    fileName: 'detect-device.template.js',
    options: moduleOptions
  })
}

// module.exports.meta = require('./package.json')
