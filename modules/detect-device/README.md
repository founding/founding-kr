# Detect device

## 테스트

### 아이폰 6S / Safari

- isMobile: true
- isDesktop: false
- os
  - name : iOS
  - version
    - version : 12.1.3
    - major: 12
    - minor: 1
    - patch: 3
- browser
  - name : safari
  - version
    - version: 12.0.0
    - major: 12
    - minor: 0
    - patch: 0

### 아이폰 6S / Chrome

- isMobile: true
- isDesktop: false
- os
  - name : iOS
  - version
    - version : 12.1.3
    - major: 12
    - minor: 1
    - patch: 3
- browser
  - name : chrome
  - version
    - version: 68.0.3440
    - major: 68
    - minor: 0
    - patch: 3440

### Mac OS / Chrome

- isMobile: true
- isDesktop: false
- os
  - name : Mac OS
  - version
    - version : 10.14.2
    - major: 10
    - minor: 14
    - patch: 2
- browser
  - name : chrome
  - version
    - version: 72.0.3626
    - major: 72
    - minor: 0
    - patch: 3626

### Mac OS / Safari

- isMobile: true
- isDesktop: false
- os
  - name : Mac OS
  - version
    - version : 10.14.2
    - major: 10
    - minor: 14
    - patch: 2
- browser
  - name : safari
  - version
    - version: 12.0.2
    - major: 12
    - minor: 0
    - patch: 2

### Mac OS / Safari

- isMobile: true
- isDesktop: false
- os
  - name : Mac OS
  - version
    - version : 10.14.2
    - major: 10
    - minor: 14
    - patch: 2
- browser
  - name : safari
  - version
    - version: 12.0.2
    - major: 12
    - minor: 0
    - patch: 2

### Mac OS / Firefox

- isMobile: true
- isDesktop: false
- os
  - name : Mac OS
  - version
    - version : 10.14
    - major: 10
    - minor: 14
- browser
  - name : firefox
  - version
    - version: 65.0.0
    - major: 65
    - minor: 0
    - patch: 0

### Mac OS / Opera

- isMobile: true
- isDesktop: false
- os
  - name : Mac OS
  - version
    - version : 10.14.2
    - major: 10
    - minor: 14
    - patch: 2
- browser
  - name : opera
  - version
    - version: 57.0.3098
    - major: 57
    - minor: 0
    - patch: 3098

### Windows 10 / IE 11

- isMobile: false
- isDesktop: true
- os
  - name : Windows 10
  - version
    - version : 10.0
    - major: 10
    - minor: 0
    - patch: 
- browser
  - name : ie
  - version
    - version: 11.0.0
    - major: 11
    - minor: 0
    - patch: 0

### Windows 10 / IE 10

- isMobile: false
- isDesktop: true
- os
  - name : Windows 10
  - version
    - version : 10.0
    - major: 10
    - minor: 0
    - patch: 
- browser
  - name : ie
  - version
    - version: 10.0.0
    - major: 10
    - minor: 0
    - patch: 0

### Windows 10 / IE 9

- isMobile: false
- isDesktop: true
- os
  - name : Windows 10
  - version
    - version : 10.0
    - major: 10
    - minor: 0
    - patch: 
- browser
  - name : ie
  - version
    - version: 9.0.0
    - major: 9
    - minor: 0
    - patch: 0

### Windows 10 / IE 8

- isMobile: false
- isDesktop: true
- os
  - name : Windows 10
  - version
    - version : 10.0
    - major: 10
    - minor: 0
    - patch: 
- browser
  - name : ie
  - version
    - version: 11.0.0
    - major: 11
    - minor: 0
    - patch: 0

### Windows 10 / Edge

- isMobile: false
- isDesktop: true
- os
  - name : Windows 10
  - version
    - version : 10.0
    - major: 10
    - minor: 0
    - patch: 
- browser
  - name : edge
  - version
    - version: 17.17134.0
    - major: 17
    - minor: 17134
    - patch: 0

### Windows 10 / Chrome

- isMobile: false
- isDesktop: true
- os
  - name : Windows 10
  - version
    - version : 10.0
    - major: 10
    - minor: 0
    - patch: 
- browser
  - name : chrome
  - version
    - version: 70.0.3538
    - major: 70
    - minor: 0
    - patch: 3538
