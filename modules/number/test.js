const mergeOptions = require('merge-options')

var split = function(number, comma = ',', level = 3) {
  let strNum = number.toString()
  let ret = []
  for (var i = 0; i < strNum.length; ++i) {
    if (i != 0 && i % level == 0) {
      ret.push(',')
    }
    ret.push(strNum[strNum.length - i - 1])
  }
  return ret.reverse().join('')
}

var suffix = function(num, divNum, suffix, withComma = true) {
  if (num / divNum >= 1) {
    let val = Math.floor(num / divNum)
    if (withComma) {
      val = split(val)
    }
    return val + suffix
  } else {
    return null
  }
}

var abbr = function(number, options = {}) {
  options = mergeOptions(
    {
      withSpace: false,
      withComma: true
    },
    options
  )
  let [num, point] = number.toString().split('.')
  num = parseInt(num)
  let ret = []

  ret.push(suffix(num, 1e12, '조', options.withComma))
  num = num % 1e12
  ret.push(suffix(num, 1e8, '억', options.withComma))
  num = num % 1e8
  ret.push(suffix(num, 1e4, '만', options.withComma))
  num = num % 1e4
  if (num == 0) num = null
  ret.push(num)

  ret = ret.filter(num => num != null)
  return ret.join(options.withSpace ? ' ' : '')
}
