# nuxt window module

윈도우의 너비와 높이를 쉽게 접근하기 위한 모듈입니다.

너비는 `this.w` 로 접근이 가능합니다.

nuxt 에서 사용할 때에는 `process.browser` 혹은 `<no-ssr>` 을 사용하여 클라이언트 사이드에서만 사용하도록 구성해야합니다. (브라우저의 window 객체에 접근하기 때문)

## w
- width : 현재 윈도우 너비
- isMobile
- isTablet
- isTablet_s
- isTablet_m
- isTablet_l
- isDesktop
- isWide
- '>mobile'
- '<mobile'
- '>tablet'
- '<tablet'
- '>tablet_s'
- '<tablet_s'
- '>tablet_m'
- '<tablet_m'
- '>tablet_l'
- '<tablet_l'
- '>desktop'
- '<desktop'
- '>wide'
- '<wide'
- mobile
- tablet
- tablet_s
- tablet_m
- tablet_l
- desktop
- wide
