import Vue from 'vue'

const data = JSON.parse('<%= JSON.stringify(options, null) %>')

const VueWindowMixin = {
  install(vue, options) {
    vue.mixin({
      data() {
        return {
          w: {
            width: 0,
            // booleans
            isMobile: this.$device ? this.$device.isMobile : false,
            isTablet: this.$device ? this.$device.isTablet : false,
            isTablet_s: false,
            isTablet_m: false,
            isTablet_l: false,
            isDesktop: this.$device ? this.$device.isDesktop : false,
            isWide: false,
            '>mobile': false,
            '<mobile': false,
            '>tablet': false,
            '<tablet': false,
            '>tablet_s': false,
            '<tablet_s': false,
            '>tablet_m': false,
            '<tablet_m': false,
            '>tablet_l': false,
            '<tablet_l': false,
            '>desktop': false,
            '<desktop': false,
            '>wide': false,
            '<wide': false,
            // min & max
            mobile: data.mobile,
            tablet: data.tablet,
            tablet_s: data.tablet_s,
            tablet_m: data.tablet_m,
            tablet_l: data.tablet_l,
            desktop: data.desktop,
            wide: data.wide
          }
        }
      },
      methods: {
        _handleResize__window(e) {
          var w = document.documentElement.clientWidth
          var h = document.documentElement.clientHeight

          this.w.width = w

          this.w.isMobile = w <= data.mobile.max
          this.w.isTablet = w >= data.tablet.min && w <= data.tablet.max
          this.w.isTablet_s = w >= data.tablet_s.min && w <= data.tablet_s.max
          this.w.isTablet_m = w >= data.tablet_m.min && w <= data.tablet_m.max
          this.w.isTablet_l = w >= data.tablet_l.min && w <= data.tablet_l.max
          this.w.isDesktop = w >= data.desktop.min && w <= data.desktop.max
          this.w.isWide = w >= data.wide.min

          this.w['>mobile'] = w > data.mobile.max
          this.w['<mobile'] = w <= data.mobile.max
          this.w['>tablet'] = w > data.tablet.max
          this.w['<tablet'] = w <= data.tablet.min
          this.w['>tablet_s'] = w > data.tablet_s.max
          this.w['<tablet_s'] = w <= data.tablet_s.min
          this.w['>tablet_m'] = w > data.tablet_m.max
          this.w['<tablet_m'] = w <= data.tablet_m.min
          this.w['>tablet_l'] = w > data.tablet_l.max
          this.w['<tablet_l'] = w <= data.tablet_l.min
          this.w['>desktop'] = w > data.desktop.max
          this.w['<desktop'] = w <= data.desktop.min
          this.w['>wide'] = w > data.wide.max
          this.w['<wide'] = w <= data.wide.min
        }
      },
      created() {
        if (process.browser) {
          window.addEventListener('resize', this._handleResize__window)
          this._handleResize__window()
        }
      },
      destroyed() {
        if (process.browser) {
          window.removeEventListener('resize', this._handleResize__window)
        }
      }
    })
  }
}

Vue.use(VueWindowMixin)
export default function(ctx, inject) {}
