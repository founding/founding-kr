const path = require('path')
const rangeData = require(path.resolve(__dirname, 'data.json'))

module.exports = function nuxtToast(moduleOptions) {
  // Register plugin
  this.addPlugin({
    src: path.resolve(__dirname, 'plugins.js'),
    ssr: true,
    fileName: 'window.template.js',
    options: Object.assign(moduleOptions, rangeData)
  })
}
