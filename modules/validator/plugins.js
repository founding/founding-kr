// https://github.com/chriso/validator.js
const validator = require('validator')

const TESTER = {
  email: 'isEmail',
  url: 'isURL',
  macaddress: 'isMACAddress',
  ip: 'isIP',
  iprange: 'isIPRange',
  fqdn: 'isFQDN',
  boolean: 'isBoolean',
  alpha: 'isAlpha',
  alphalocales: 'isAlphaLocales',
  alphanumeric: 'isAlphanumeric',
  alphanumericlocales: 'isAlphanumericLocales',
  numeric: 'isNumeric',
  port: 'isPort',
  lowercase: 'isLowercase',
  uppercase: 'isUppercase',
  ascii: 'isAscii',
  fullwidth: 'isFullWidth',
  halfwidth: 'isHalfWidth',
  variablewidth: 'isVariableWidth',
  multibyte: 'isMultibyte',
  surrogatepair: 'isSurrogatePair',
  int: 'isInt',
  float: 'isFloat',
  floatlocales: 'isFloatLocales',
  decimal: 'isDecimal',
  hexadecimal: 'isHexadecimal',
  divisibleby: 'isDivisibleBy',
  hexcolor: 'isHexColor',
  isrc: 'isISRC',
  md5: 'isMD5',
  hash: 'isHash',
  jwt: 'isJWT',
  json: 'isJSON',
  empty: 'isEmpty',
  length: 'isLength',
  bytelength: 'isByteLength',
  uuid: 'isUUID',
  mongoid: 'isMongoId',
  after: 'isAfter',
  before: 'isBefore',
  creditcard: 'isCreditCard',
  identitycard: 'isIdentityCard',
  isin: 'isISIN',
  isbn: 'isISBN',
  issn: 'isISSN',
  mobilephone: 'isMobilePhone',
  mobilephonelocales: 'isMobilePhoneLocales',
  postalcode: 'isPostalCode',
  postalcodelocales: 'isPostalCodeLocales',
  currency: 'isCurrency',
  iso8601: 'isISO8601',
  rfc3339: 'isRFC3339',
  iso31661alpha2: 'isISO31661Alpha2',
  iso31661alpha3: 'isISO31661Alpha3',
  base64: 'isBase64',
  datauri: 'isDataURI',
  magneturi: 'isMagnetURI',
  mimetype: 'isMimeType',
  latlong: 'isLatLong'
}

function validate(tester, input, testOptions = {}) {
  if (typeof input === 'string') {
    if (typeof testOptions.minLength === 'number') {
      if (input.length < testOptions.minLength) {
        // minLength 충족 X
        return false
      }
    }
    if (typeof testOptions.maxLength === 'number') {
      if (input.length > testOptions.maxLength) {
        // maxLength 충족 X
        return false
      }
    }
  } else if (typeof input === 'number') {
    if (typeof testOptions.min === 'number') {
      if (input < testOptions.min) {
        // min 충족 X
        return false
      }
    }
    if (typeof testOptions.max === 'number') {
      if (input > testOptions.max) {
        // max 충족 X
        return false
      }
    }
  } else {
    return false
  }

  if (typeof tester === 'string') {
    var testerValue = TESTER[tester]

    if (typeof testerValue === 'string') {
      var func = validator[testerValue]
      return func(input)
    } else if (typeof testerValue === 'function') {
      return testerValue(input)
    } else {
      throw new Error(`Not valid validator test name: '${tester}'`)
    }
  } else if (typeof tester === 'function') {
    return tester(input)
  } else if (tester instanceof RegExp) {
    if (typeof input === 'number') input = input + ''
    return tester.test(input)
  }

  return false
}

export default function(ctx, inject) {
  inject('validator', {
    v: validate
  })
}
