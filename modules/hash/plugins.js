const crypto = require('crypto')
const bs58 = require('bs58')

export default function(ctx, inject) {
  var makeHash = function(str, withBuildId) {
    if (process.env.BUILDID == null) {
      throw new Error('process.env.BUILDID is not set')
    }

    if (withBuildId) {
      str = process.env.BUILDID + str
    }
    return bs58.encode(
      new Buffer(
        crypto
          .createHash('md')
          .update(str)
          .digest('base64')
      )
    )
  }

  inject('hash', {
    filename: (str, withBuildId = true) => {
      return makeHash(str, withBuildId)
    },
    make: makeHash
  })
}
