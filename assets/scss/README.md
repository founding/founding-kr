# SCSS #

# 디렉토리 구조 #
- `abstracts/` : 변수 설정을 위한 파일들이 위치해 있다.
- `base/` : 애니메이션 정의, 엘리먼트 초기화, mixins, functions 등 기본 sass 관련 설정들이 위치해 있다.
- `components/` : 컴포넌트 스타일 설정이 위치해 있다.
- `layouts/` : 레이아웃 스타일 설정이 위치해 있다. 컴파일 시 이 디렉토리에 있는 파일들이 `source` 가 된다.
- `override/` : 현재 사용되고 있지 않음. `vendors/`의 플러그인을 override 하기 위해 사용된다.
- `pages/` : 페이지 스타일 설정이 위치해 있다. 컴파일 시 이 디렉토리에 있는 파일들이 `source` 가 된다.
- `vendors/` : 외부 스타일이나 CSS 라이브러리가 위치한다.

# import 순서 #

외부에서(vue 파일 혹은 컴파일 시) 사용되는 디렉토리는 단 2가지 이다. `layouts/`, `pages/` 가 그것이다.
`layouts/`는 레이아웃과 관련된 모든 컴포넌트를 추가하고, `pages/`는 컴포넌트를 제외한 최소한으로 동작하기 위한 파일들을 추가한다.

아래 자세한 내용 확인.

## layouts/ ##

Vue 스타일의 메인이 되는 파일로 레이아웃별로 디렉토리가 나뉘어져 있으며, 따라서 레이아웃별로 css 파일이 생성된다.

순서는 아래와 같다.

1. layouts/preset
    1. abstracts/main : 변수 설정
    2. base/main : mixins, animation 설정
    3. vendors/main : 외부 플러그인/라이브러리 추가
    4. override/main : 외부 플러그인/라이브러리 override
2. layouts/common : 레이아웃 공통 요소 추가
    1. layouts/mixins : base/main에 위치하기에는 애매한 레이아웃 전용 mixin
    2. layouts/common/* : 그 외 레이아웃에서 필요하다고 생각되는 스타일
3. 레이아웃 변수/스타일 설정 : 변수 값 재설정 가능
4. layouts/postset : 재 설정된 변수 값을 실제 디스플레이 될 곳에 적용한다.
    1. components/main : 컴포넌트 적용

레이아웃별로 파일을 나눈 이유는 레이아웃별로 변수를 재지정하기 위한 이유가 가장 크다.

예를 들어, 변수 `$theme-color` 색상이 `red`이고, 컴포넌트에서는 모두 `$theme-color` 를 사용한다고 할 때, `dark` 레이아웃에서는 이를 `darkred` 로 만들고 싶다. 그렇다고 해서 다른 레이아웃에서는 `red` 색상을 유지하고 싶을 때, 이러한 방식이 빛을 발한다. 같은 컴포넌트 더라도 원하는 레이아웃에서는 색상을 변경할 수 있도록 하는 것이다. 이를 응용하면 이벤트가 있는 날, 예를 들어 크리스마스 같은 날엔 컴포넌트 색상을 크리스마스에 어울리는 색상으로 적용이 가능 할 것이다.

## pages/ ##

페이지 별로 스타일을 정의한다.

순서는 아래와 같다.
1. pages/common
    1. abstracts/main : 변수 설정
    2. base/mixins : mixins, animation 설정
    3. layouts/mixins : 레이아웃 mixins

여기에 위치한 파일들은 전체 레이아웃을 구정하는데 critical 하지 않다. 오직 부수적인 요소로만 사용될 뿐이다.
같은 레이아웃이더라도 `width` 를 다르게 한다던가, override 해야 할 것이 있을 때 주로 사용한다.
페이지 단위기 때문에 다른 스타일에 오염될 염려도 없으므로 유일하게 `!important` 사용이 가능하다.

# 규칙 #



