module.exports = [
  // 확장자 없이 입력
  'layouts/default',
  'layouts/article/base',
  'layouts/base/base',
  'layouts/base/simple',
  'layouts/board/base',
  'layouts/invest/base',
  'layouts/support/base',
  'layouts/terms/base',
  'layouts/terms/document',
  'pages/home',
  'pages/auth/forgot',
  'pages/auth/login',
  'pages/auth/signup',
  'pages/invest/index',
  'pages/invest/view/id',
  'pages/terms/index',
  'pages/terms/document',
  'pages/support/contact'
]
