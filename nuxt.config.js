const pkg = require('./package')
const util = require('util')
const fs = require('fs')

const CSSNameEncrypter = require('css-name-encrypter')
const cssName = new CSSNameEncrypter()
const cssClasses = require('./assets/data/classes.json')
/**
 * 기존 BEM 형식을 camelCase 형태로 변환한다.
 * element 지시자 __ 는 그대로 사용한다.
 * modifier 지시자 -- 는 '_' 으로 변환한다.
 * ke-bab 스타일로 붙혀진 이름을 camelCase 형태로 변환한다.
 */
function toCamelCase(str) {
  // 현재 사용 안함
  // const pattern1 = /__(\w|$)/g;
  const pattern2 = /--(\w|$)/g
  const pattern3 = /-(\w|$)/g
  str = str.toLowerCase()
  // str = str.replace(pattern1, (dashChar, char) => {
  //   return '__' + char
  // })
  str = str.replace(pattern2, (dashChar, char) => {
    return '_' + char
  })
  str = str.replace(pattern3, (dashChar, char) => {
    return char.toUpperCase()
  })
  return str
}
function cssModule_getLocalIdent(context, localIdentName, localName, options) {
  if (cssClasses[localName] === undefined) {
    console.log(
      'Not exists selector: ' + localName + '->' + cssClasses[localName]
    )
  }
  return cssClasses[localName] || '_undefined'
}

console.log(process.env.NODE_ENV)
module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://cdn.jsdelivr.net/npm/animate.css@3.5.1'
      }
    ],
    script: [
      {
        src: 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
        type: 'text/javascript'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: ['@/plugins/vue-awesome', '@/plugins/vue2-touch-events'],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '~/modules/detect-device',
    '~/modules/number',
    '~/modules/validator',
    '~/modules/random',
    '~/modules/window'
  ],
  /*
  ** Axios module configuration
  */
  axios: {},

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    transpile: [/^vue-awesome/],
    splitChunks: {
      layouts: true
    },
    // analyze: true,
    loaders: {
      cssModules: {
        modules: true,
        importLoaders: 2,
        localIdentName: '[local]',
        getLocalIdent:
          process.env.NODE_ENV == 'development'
            ? undefined // cssModule_getLocalIdent
            : cssModule_getLocalIdent
      }
    },
    postcss: {
      plugins: {
        cssnano: {
          preset: ['default', { discardEmpty: false }]
        }
      }
    },
    extend(config, ctx) {
      if (ctx.isDev) {
        console.log(`Dev`)
      } else {
        console.log(`Production`)
      }
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        // config.module.rules.push({
        //   enforce: 'pre',
        //   test: /\.(js|vue)$/,
        //   loader: 'eslint-loader',
        //   exclude: /(node_modules)/
        // })
      }

      if (ctx.isDev || ctx.isClient || ctx.isServer) {
        console.log(
          '!! svg 파일 동적 로드를 위해 url-loader에서 svg 확장자는 html-loader로 대체됩니다.'
        )

        /**
         * 아래 부분에서 png|jpe?g|gif|svg|webp 이 부분이 제거되어야 함.
         * 그렇지 않으면 svg부분이 text로 출력됨
         * TODO: 자동으로 svg 부분을 찾아 제거할 것
         */

        config.module.rules[8] = {}
        config.module.rules.push({
          test: /\.(png|jpe?g|gif|webp)$/,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 1000,
                name: '[path][name].[ext]'
              }
            }
          ]
        })
        config.module.rules.push({
          test: /\.svg$/,
          use: [
            {
              loader: 'html-loader',
              options: {
                minimize: true
              }
            }
          ]
        })
        // console.log(config.module.rules)
        // console.log(JSON.stringify(config.module.rules, null, 2))
      }
    }
  }
}
