# Founding

> 부동산 경매 P2P 크라우드 펀딩

# CSS 관련 #

## CSS Modules/Encryption ##

### 사용되는 모듈 ###

- [css-name-encrypter](https://gitlab.com/basic-modules/css-name-encrypter)
- [sass-compile-postcss](https://gitlab.com/basic-modules/sass-compile-postcss)

### 설명 ###

Vue 작성 시 CSS Modules를 이용해 클래스 명을 참조한다.
이 방법을 사용하는 이유는 두 가지가 있는데 첫번째는 CSS 클래스 이름 길이를 줄이기 위함, 두번째는 CSS 클래스 이름 노출을 최소화 시키기 위함이다.

실제로, 홈화면 테스트 결과 약 10% 가량의 파일 크기를 줄일 수 있었다.
파일 내용이 많아지고, 클래스 선택자를 많이 사용할 수록 이 비율은 증가할 것이다.

|                | Dev         | Prod        |
|----------------|-------------|-------------|
| Non-encryption | 78919 Bytes | 76375 Bytes |
| Encryption     | 71282 Bytes | 68738 Bytes |

스타일이 `SCSS`로 작성되면, 직접 `npm run scss-compile` 명령을 통해 컴파일을 해야한다. 컴파일 한 후에는 `/static` 위치에 `css` 파일이 생성되고, `layout`, `pages` 등은 해당 위치를 `head`로 설정하여 스타일 시트를 사용한다.

`/sass-compile.js` 스크립트에서 모든 과정이 진행되며, 새로운 파일을 추가할 경우 해당 파일 내부에서 `SCSS_SOURCES` 변수에 할당하면 된다.

컴파일 과정은 `소스파일.scss` -> `sass` 컴파일 -> `postcss` 변환 및 encryption -> `타겟파일.css` 순서대로 진행된다.
`sass` 컴파일 및 `postcss` 사용을 위해 `sass-compile-postcss` 모듈이 사용되며, encryption 및 encrypted 된 데이터를 가지고 있는 `css-name-encrypter` 모듈이 옵션으로 들어간다.

CSS 셀럭터와 encrypted된 이름은 1:1 매칭되며, `JSON` 형식으로 저장된다.
저장되는 위치는 `/assets/data/classes.json` 이다.

### nuxt.config.js ###

Vue 에서 CSS 셀렉터를 encrypted된 이름으로 참조하기 위해서 `build.loaders.cssModules` 옵션의 `getLocalIdent` 를 재정의 하였는데, 재 정의된 함수는 `/assets/data/classes.json`파일을 이용해 CSS 셀럭터와 매칭된 encrypted된 이름을 가져오는 역할을 한다.

## 파일 구조 ##

- `/assets/scss/` : 모든 `scss` 파일은 이 아래에 위치한다.
- `/static/css/` : 모든 변환된 `css` 파일은 이 아래에 위치한다.

## 규칙 ##
자세한 파일 구조 및 규칙은 [이 문서](./assets/scss/README.md)를 참조하면 된다.
