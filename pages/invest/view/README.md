# invest/view/id

## 화면 구성

화면 구성은 크게 상단, 컨텐츠, 하단으로 나눈다.
*상단*에는 썸네일과 현재 펀딩 규모,
*컨텐츠*에는 상품 정보,
*하단*에는 투자 유의사항과 리뷰 등이 위치한다.

### 사용되는 컴포넌트
- thumbnail-carousel
  - `if >tablet_m` 넓은 면적과 오른쪽에 투자 정보가 한 화면에 표기된다.
  - `else` 스크롤 하며 볼 수 있는 투자 정보가 나타난다.
- invest-form 
  - `if >tablet_m` sticky 형태로 오른쪽에 붙어서 스크롤과 함꼐 이동한다.
  - `else` 하단부에 버튼이 위치하고 floating 형태로 폼이 나타난다.   


### 레이아웃
