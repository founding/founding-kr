/**
 * random.js
 * 랜덤함수를 저장하기 위한 스토어이다.
 * 랜덤 생성은 modules/random 모듈에서 담당한다.
 */

export const state = () => ({
  history: {}
})

export const mutations = {
  /**
   * add
   * 랜덤으로 생성된 수를 scope에 해당하는 history에 저장한다
   *
   * payload.scope<string>: history 위치
   * payload.num<number>: 추가할 숫자
   */
  add(state, payload = {}) {
    if (Array.isArray(state.history[payload.scope])) {
      state.history[payload.scope].push(payload.num)
    } else {
      state.history[payload.scope] = [payload.num]
    }
  },
  /**
   * init
   * scope에 해당하는 history를 초기화한다.
   */
  init(state, scope) {
    state.history[scope] = []
  }
}
