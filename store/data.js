var defineStates
var defineMutations
var defineActions

if (process.server) {
  const fs = require('fs')
  function fileToJson(path) {
    return JSON.parse(fs.readFileSync(path).toString())
  }
  defineStates = () => ({
    headerItems: '',
    footerItems: ''
  })

  defineMutations = {
    header(state) {
      state.headerItems = fileToJson('./assets/data/header.json')
    },
    simpleHeader(state) {
      state.headerItems = fileToJson('./assets/data/simpleHeader.json')
    },
    supportHeader(state) {
      state.headerItems = fileToJson('./assets/data/supportHeader.json')
    },
    footer(state) {
      state.footerItems = fileToJson('./assets/data/footer.json')
    }
  }

  defineActions = {
    base(context) {
      context.commit('header')
      context.commit('footer')
    },
    simple(context) {
      context.commit('simpleHeader')
      context.commit('footer')
    },
    support(context, basename) {
      // basename은 왼쪽 위 드롭다운 메뉴 이름을 설정한다.
      context.commit('supportHeader')
      context.state.headerItems[0].name = basename
      context.commit('footer')
    }
  }
}
if (process.client) {
  defineStates = () => ({})
}

export const state = defineStates

export const mutations = defineMutations

export const actions = defineActions
