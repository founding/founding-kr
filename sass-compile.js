const fs = require('fs')
const shell = require('shelljs')
const path = require('path')
const sass = require('node-sass')
const autoprefixer = require('autoprefixer')
const postcss = require('postcss')
const precss = require('precss')
const sassCompilePostcss = require('sass-compile-postcss')

const CSSNameEncrypter = require('css-name-encrypter')
const cssNameEncrypter = new CSSNameEncrypter()

const SCSS_PREFIX = path.resolve(__dirname, 'assets/scss')
const TARGET_PREFIX = path.resolve(__dirname, 'static/css')
const CSS_TARGET = path.resolve(__dirname, 'assets/data/classes.json')

const OPTIONS = {
  encrypt: true,
  postcss: true,
  postcssModules: true,
  cssnano: true
}
if (process.env.NODE_ENV == 'production') {
  OPTIONS.encrypt = true
  // do something in production
} else {
  OPTIONS.encrypt = false
  // do something in development
}

console.log(process.env.NODE_ENV)
console.log(OPTIONS)

const SCSS_SOURCES = require('./sass-compile.config.js')

function finishCallback() {
  fs.writeFileSync(CSS_TARGET, JSON.stringify(cssNameEncrypter.toJSON()))
  fs.unlinkSync(path.resolve(__dirname, './undefined.json'))
}

let count = 0
SCSS_SOURCES.forEach(filename => {
  let source = path.resolve(SCSS_PREFIX, filename + '.scss')
  let target = path.resolve(TARGET_PREFIX, filename + '.css')

  count++
  sassCompilePostcss.compile(
    source,
    target,
    {
      plugins: [autoprefixer],
      postcss: OPTIONS.postcss,
      postcssModules: OPTIONS.postcssModules,
      cssnano: OPTIONS.cssnano,
      postcssModulesOptions: {
        generateScopedName: function(name, filename, css) {
          // encrypt 과정
          if (OPTIONS.encrypt) {
            return cssNameEncrypter.encrypt(name)
          } else {
            return name
          }
        }
      }
    },
    function() {
      count--
      if (count == 0) {
        finishCallback()
      }
    }
  )
})
