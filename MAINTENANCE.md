# 유지보수 

## 커밋 분류

- [Layout] Vue, SCSS 레이아웃 관련
- [Page] Vue, SCSS 페이지 관련
- [Component] Vue, SCSS 컴포넌트 관련
- [Data] 데이터 관련
- [Static] Static 파일 관련
- [Nuxt] nuxt 설정 관련
- [Plugin] 플러그인 관련
- [Module] 모듈 관련
- [Middleware] 미들웨어 관련
- [Doc] 문서화 관련, Vue 주석 문서도 포함
- [Server] Express 서버 설정 관련

## Issue workflow

- 종류 : ~feature, ~bug, ...
- 주제 : ~plan, ~api, ~issue, ~design, ...
- 단계 : ~devops:create, ~devops:release ~...
- 중요도 : ~P1, ~P2, ~P3, ~P4
- 엄격도 : ~S1, ~S2, ~S3, ~S4

### 종류
- ~feature
- ~bug
- ~idea

### 주제
- ~plan
- ~api
- ~issue
- ~ui
- ~documentation
- ~secure
- ~production
- ~component
- ~layout
- ~middleware
- ~module
- ~server

### 단계
- ~devops:manage
- ~devops:plan
- ~devops:create
- ~devops:verify
- ~devops:package
- ~devops:release
- ~devops:configure
- ~devops:monitor
- ~devops:secure

### 중요도

| Label | Meaning         | Defect SLA (applies only to ~bug and ~security defects)                                                    |
|-------|-----------------|----------------------------------------------------------------------------|
| ~P1   | Urgent Priority | The current release + potentially immediate hotfix to GitLab.com (30 days) |
| ~P2   | High Priority   | The next release (60 days)                                                 |
| ~P3   | Medium Priority | Within the next 3 releases (approx one quarter or 90 days)                 |
| ~P4   | Low Priority    | Anything outside the next 3 releases (more than one quarter or 120 days)   |

### 엄격도
| Label | Meaning           | Impact on Functionality                               | Example |
|-------|-------------------|-------------------------------------------------------|---------|
| ~S1   | Blocker           | Outage, broken feature with no workaround             | Unable to create an issue. Data corruption/loss. Security breach. |
| ~S2   | Critical Severity | Broken Feature, workaround too complex & unacceptable | Can push commits, but only via the command line. |
| ~S3   | Major Severity    | Broken Feature, workaround acceptable                 | Can create merge requests only from the Merge Requests page, not through the Issue. |
| ~S4   | Low Severity      | Functionality inconvenience or cosmetic issue         | Label colors are incorrect / not being displayed. |

#### 엄격도에 따른 영향
| Severity | Affected Customers/Users                                            | GitLab.com Availability                            |  Performance Degradation     |
|----------|---------------------------------------------------------------------|----------------------------------------------------|------------------------------|
| ~S1      | >50% users affected (possible company extinction level event)       | Significant impact on all of GitLab.com            |                              |
| ~S2      | Many users or multiple paid customers affected (but not apocalyptic)| Significant impact on large portions of GitLab.com | Degradation is guaranteed to occur in the near future |
| ~S3      | A few users or a single paid customer affected                      | Limited impact on important portions of GitLab.com | Degradation is likely to occur in the near future     |
| ~S4      | No paid users/customer affected, or expected to in the near future  | Minor impact on GitLab.com                         | Degradation _may_ occur but it's not likely           |
